var pacientes = document.querySelectorAll(".paciente");
console.log(pacientes);

for(var i=0; i < pacientes.length; i++){

  var paciente = pacientes[i];
  var tdPeso = paciente.querySelector(".info-peso");
  var peso = tdPeso.textContent;

  var tdAltura = paciente.querySelector(".info-altura");
  var altura = tdAltura.textContent;

  var tdimc = paciente.querySelector(".info-imc");

  alturaEhValido = validaAltura(altura);

  pesoEhValido = validaPeso(peso);

  if(!pesoEhValido){
    tdimc.textContent = "Peso Inválido!";
    paciente.classList.add("paciente-invalido");
  }

  if(!alturaEhValido){
    tdimc.textContent = "Altura Inválido!";
    paciente.classList.add("paciente-invalido");
  }

  if(pesoEhValido == true && alturaEhValido == true){
    tdimc.textContent = calculaImc(peso, altura);
  }

}

function validaPeso(peso){
  if(peso >= 0 && peso < 1000){
    return true;
  } else {
    return false;
  }
}

function validaAltura(altura){
  if(altura >= 0 && altura <= 3.00){
    return true;
  } else {
    return false;
  }
}

function calculaImc(peso, altura){
  var imc = 0;
  imc = peso / (altura * altura);
  return imc.toFixed(2);
}
