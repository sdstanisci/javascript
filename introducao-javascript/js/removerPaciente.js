
var pacientes = document.querySelectorAll(".paciente");

var tabela = document.querySelector("table");

tabela.addEventListener("dblclick", function(event){
  event.target.parentNode.classList.add("fadeOut"); //remove a tr

  setTimeout(function(){
    event.target.parentNode.remove();
  }, 500);
});

//pacientes.forEach(function(paciente){
//  paciente.addEventListener("dblclick", function(){
      //Usei o This, por conta do paciente estar fora do contexto
//      this.remove();
//  });
//});
