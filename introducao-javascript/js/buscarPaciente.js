var botao = document.querySelector("#buscar-paciente");

botao.addEventListener("click", function(){

    var xhr = new XMLHttpRequest();
    xhr.open("get","https://api-pacientes.herokuapp.com/pacientes");

    xhr.addEventListener("load", function(){
      var erroAjax = document.querySelector("#erro-ajax");
       if(xhr.status == 200){
         var resposta = xhr.responseText;
         var pacientes  = JSON.parse(resposta);

         pacientes.forEach(function(paciente){
            adicionaPacienteNaTabela(paciente);
         })
         erroAjax.classList.add("invisivel");
       } else {
         console.log("Erro na Pagina: " + xhr.status);
         console.log("Retorno: " + xhr.responseText);

         erroAjax.classList.remove("invisivel");
       }

    });
    xhr.send();
});
