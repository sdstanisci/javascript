class NegociacaoController{

  constructor(){
    let $ = document.querySelector.bind(document);
    this._inputData = $('#data');
    this._inputQuantidade = $('#quantidade');
    this._inputValor = $('#valor');
    //this._listaNegociacoes = new ListaNegociacoes(model =>
        // quando se usa arrow function o escopo eh lexico(statico) e o this entende que eh o negociacaoController e nao o ListaNegociacoes
      //  this._negociacoesView.update(model));

    let self = this;
    // Proxy eh uma copia do modelo para criar uma camada entre o modelo e o controller para poder fazer alguns tratamentos
    this._listaNegociacoes = new Proxy(new ListaNegociacoes(), {
      // o get eh chamado sempre que tentar ler qualquer propriedade do objeto
      // target: quando chamado o target eh uma referencia ao objeto original que esta sendo encapsulado pelo Proxy
      // prop: a propriedade que esta sendo acessada
      // receiver: a referencia ao proprio proxy
      get: function(target, prop, receiver){
            if(['adiciona', 'esvazia'].includes(prop) && typeof(target[prop]) == typeof(Function)){
              return function(){
                console.log(`interceptando ${prop}`);
                Reflect.apply(target[prop], target, arguments);
                self._negociacoesView.update(target);
              }
            }
            return Reflect.get(target, prop, receiver);
      }
      //set: function(target, prop, value, receiver){
      //      console.log(`valor do objeto anterior: ${target[prop]}, novo objeto: ${value}`);
      //      return Reflect.get(target, prop, value, receiver);
      //}

    });

    this._negociacoesView = new NegociacoesView($('#negociacoesView'));

    this._negociacoesView.update(this._listaNegociacoes);

    this._mensagem = new Mensagem();
    this._mensagemView = new MensagemView($('#mensagemView'));
    this._mensagemView.update(this._mensagem);
  }
    //console.log(typeof(this._inputData.value));
    // os ... significa que sao os 3 elementos do array que o split vai retornar quando cortar o "-"

    adiciona(event) {

        event.preventDefault();
        this._listaNegociacoes.adiciona(this._criaNegociacao());

        this._mensagem.texto = 'Negociação adicionada com sucesso!';
        this._mensagemView.update(this._mensagem);
        this._limpaFormulario();
    }

    apaga(){
      this._listaNegociacoes.esvazia();

      this._mensagem.texto = 'Negociações apadas com sucesso';
      this._mensagemView.update(this._mensagem);
    }

    _criaNegociacao() {

        return new Negociacao(
            DateHelper.textoParaData(this._inputData.value),
            this._inputQuantidade.value,
            this._inputValor.value);
    }

    _limpaFormulario() {

        this._inputData.value = '';
        this._inputQuantidade.value = 1;
        this._inputValor.value = 0.0;
        this._inputData.focus();
    }
}
