class NegociacoesView extends View{

  constructor(elemento){
    super(elemento);
  }

  template(model){
    //eh usado o acento circunflexo para poder retornar um String pulando linhas
    return  `
    <table class="table table-hover table-bordered">
        <thead>
            <tr>
                <th>DATA</th>
                <th>QUANTIDADE</th>
                <th>VALOR</th>
                <th>VOLUME</th>
            </tr>
        </thead>

        <tbody>
          ${model.negociacoes.map((n) => {
              return `
                <tr>
                  <td>${DateHelper.dataParaTexto(n.data)}</td>
                  <td>${n.quantidade}</td>
                  <td>${n.valor}</td>
                  <td>${n.volume}</td>
                </tr>
              `
          }).join('')}
        </tbody>

        <tfoot>
          <td colspan="3"></td>
          <td>${
            model.negociacoes.reduce((total, n) => total + n.volume, 0.00)

            //pode ser feito dessa forma tmb
            //(function(){
            //  let total = 0;
            //  model.negociacoes.forEach(n => total += n.volume);
            //  return total;
            //})()
            //preciso do () para invocar a funcao de cima IIFE Invocando funcao expression
          }

          </td>
        </tfoot>
    </table> `;
  }
}
