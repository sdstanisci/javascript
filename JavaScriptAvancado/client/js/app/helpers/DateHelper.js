class DateHelper{

  constructor(){
    throw new Error('DateHelper não pode ser instanciado.');
  }
  // a => (aero function) substitui a palavra function da funcao anonima
  static textoParaData(texto){
    console.log(texto)
    return new Date(...
      texto.split('-').map((item, indice) => {if(indice == 1) return item - 1; return item;}
    ));
  }

  static dataParaTexto(data){

    //validando o formato da data
    if(/\d{4}-\d{2}-\d{2}/.test(data)){
      throw new Error('Deve estar no formato aaaa-mm-dd');
    }

    return `${data.getDate()}/ ${data.getMonth()+1}/ ${data.getFullYear()}`;
  }
}
