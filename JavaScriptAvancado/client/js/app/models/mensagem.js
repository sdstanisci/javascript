class Mensagem{
  constructor(texto=''){
    // se nao enviar nada no paramtro texto='' eh atribuido vazio 
    this._texto = texto;
  }

  get texto(){
    return this._texto;
  }

  set texto(texto){
    this._texto = texto;
  }
}
